package com.example.project1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        CTAButton.setOnClickListener {
            val randomNumber: Int = (-100..100).random()
            textOutput.text =
                randomNumber.toString() + if (randomNumber % 5 == 0 && randomNumber / 5 > 0) " Yes" else " No"
        }
    }
}